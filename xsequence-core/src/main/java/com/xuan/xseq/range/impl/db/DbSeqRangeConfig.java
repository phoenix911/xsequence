package com.xuan.xseq.range.impl.db;

import com.xuan.xseq.range.AbstractSeqRangeConfig;

import javax.sql.DataSource;

/**
 * DB区间配置
 * Created by xuan on 2018/4/29.
 */
public class DbSeqRangeConfig extends AbstractSeqRangeConfig {

    /**
     * 表名前缀，为防止数据库表名冲突，默认带上这个前缀
     */
    private final static String TABLENAME_PREFIX = "x_sequence_";

    /**
     * DB来源
     */
    private DataSource dataSource;

    /**
     * 表名，默认range
     */
    private String tableName = "range";

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public String getTableName() {
        return TABLENAME_PREFIX + tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

}
